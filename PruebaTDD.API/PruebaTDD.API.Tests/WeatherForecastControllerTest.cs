﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using PruebaTDD.API.Controllers;

namespace PruebaTDD.API.Tests
{
    public class WeatherForecastControllerTest
    {
        [Fact]
        public void Get_ReturnsAList_WithWeatherForecasts()
        {
            var logger = new Mock<ILogger<WeatherForecastController>>();

            var sut = new WeatherForecastController(logger.Object);

            var resultado = sut.Get();

            resultado.Should().BeAssignableTo<IEnumerable<WeatherForecast>>();
        }

    }
}
